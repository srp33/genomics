import os, sys, glob, shutil, collections, random
from utilities import *
from VariantUtilities import *

annotatedVcfFilePatterns = sys.argv[1].split(",")
acceptedImpactLevels = set(sys.argv[2].split(","))
localMafThreshold = float(sys.argv[3])
outFullTableFilePath = sys.argv[4]
outFilteredTableFilePath = sys.argv[5]
outSummaryFilePath = sys.argv[6]

def parseVariants():
    annotatedVcfFilePaths = []
    for annotatedVcfFilePattern in annotatedVcfFilePatterns:
        annotatedVcfFilePaths.extend(glob.glob(annotatedVcfFilePattern))

    positionVariantDict = {}
    allSampleIDs = set()

    for annotatedVcfFilePath in annotatedVcfFilePaths:
        print "Loading data from %s" % annotatedVcfFilePath
        annotatedVcfFile = open(annotatedVcfFilePath)
        line = annotatedVcfFile.readline()
        while line.startswith("##"):
            line = annotatedVcfFile.readline()

        sampleIDs = line.rstrip().split("\t")[9:]
        for x in sampleIDs:
            allSampleIDs.add(x)

        lineCount = 0
        for line in annotatedVcfFile:
            lineCount += 1
            if lineCount % 10000 == 0:
                print lineCount

            positionID, chromosome, position, externalID, ref, variantType, genotypeValuesList, geneAnnotationDict, variantAnnotationDict = parseVcfLine(line, sampleIDs, acceptedImpactLevels)
            #if chromosome != "chr22":
            #    continue

            if positionID not in positionVariantDict:
                positionVariantDict[positionID] = []

            for i in range(len(genotypeValuesList)):
                genotypeValues = genotypeValuesList[i]

                description = genotypeValues[1]
                alt = genotypeValues[0]

                sampleID = sampleIDs[i]

                variantDict = {}
                variantDict["Chromosome"] = chromosome
                variantDict["Position"] = position
                variantDict["SampleID"] = sampleID
                variantDict["Ref"] = ref
                variantDict["Alt"] = alt
                variantDict["ExternalID"] = externalID
                variantDict["Description"] = description
                variantDict["VariantType"] = variantType
                variantDict["VariantSubType"] = getVariantSubType(ref, alt, variantType)

                variantDict.update(geneAnnotationDict)

                for metaKey in ("VariantEffect", "VariantImpactLevel", "SIFT", "Polyphen", "MutAssr", "CondelScore", "CondelCall", "GeneID", "GeneSymbol", "Strand", "Pathways", "LocalMAF", "dbSNP_Common", "1000G_Common", "ESP_Common", "TCGA_Common"):
                    if variantAnnotationDict.has_key(alt) and variantAnnotationDict[alt].has_key(metaKey):
                        variantDict[metaKey] = variantAnnotationDict[alt][metaKey]

                positionVariantDict[positionID].append(variantDict)

        annotatedVcfFile.close()

    saveVariantsToFile(positionVariantDict, outFullTableFilePath)
    printFlush("Number of samples: %i" % len(allSampleIDs), outSummaryFilePath)
    printStats("Before any filtering", positionVariantDict, allSampleIDs)

    removeMajorAllele(positionVariantDict)
    printStats("After removing major allele and variants for which all genotypes are missing", positionVariantDict, allSampleIDs)

    for positionID in positionVariantDict.keys():
        filterPositionByKeyValue(positionVariantDict, positionID, "dbSNP_Common", "True")
    printStats("After removing remaining variants that are common in dbSNP", positionVariantDict, allSampleIDs)

    for positionID in positionVariantDict.keys():
        filterPositionByKeyValue(positionVariantDict, positionID, "1000G_Common", "True")
    printStats("After removing remaining variants that are common in 1000 Genomes", positionVariantDict, allSampleIDs)

    for positionID in positionVariantDict.keys():
        filterPositionByKeyValue(positionVariantDict, positionID, "ESP_Common", "True")
    printStats("After removing remaining variants that are common in Exome Sequencing Project", positionVariantDict, allSampleIDs)

    for positionID in positionVariantDict.keys():
        filterPositionByKeyValue(positionVariantDict, positionID, "TCGA_Common", "True")
    printStats("After removing remaining variants that are common in TCGA breast data", positionVariantDict, allSampleIDs)

    for positionID in positionVariantDict.keys():
        filterPositionByKeyValue(positionVariantDict, positionID, "LocalMAF", "0.0000")
        filterPositionByKeyValueLessThan(positionVariantDict, positionID, "LocalMAF", localMafThreshold)
    printStats("After removing remaining variants that have MAF > %.4f or that are all identical in this data set" % localMafThreshold, positionVariantDict, allSampleIDs)

    removeVariantsByFunction(positionVariantDict)
    printStats("After filtering for sequence effect", positionVariantDict, allSampleIDs)
    removeNonPathogenicVariants(positionVariantDict)
    printStats("After filtering for conservation/function", positionVariantDict, allSampleIDs)

    saveVariantsToFile(positionVariantDict, outFilteredTableFilePath)

def removeMajorAllele(positionVariantDict):
    for position in positionVariantDict.keys():
        freqDict = {"HC": 0, "HT": 0, "HR": 0, "UK": 0}
        for variantDict in positionVariantDict[position]:
            freqDict[variantDict["Description"]] += 1

        majorAlleleDescription = getMajorAlleleDescription(freqDict)
        filterPositionByKeyValue(positionVariantDict, position, "Description", majorAlleleDescription)

def removeVariantsByFunction(positionVariantDict):
    for positionID in positionVariantDict.keys():
        filterPositionByKeyValue(positionVariantDict, positionID, "VariantEffect", None)

def removeNonPathogenicVariants(positionVariantDict):
    for positionID in positionVariantDict.keys():
        variantDictList = []
        for variantDict in positionVariantDict.pop(positionID):
            if ("VariantImpactLevel" in variantDict and variantDict["VariantImpactLevel"] == "HIGH") or variantDict["VariantType"] == "INDEL" or ("CondelCall" in variantDict and variantDict["CondelCall"] == "deleterious"):
                variantDictList.append(variantDict)

        if len(variantDictList) > 0:
            positionVariantDict[positionID] = variantDictList

def saveVariantsToFile(positionVariantDict, outFilePath):
    keys = set()
    for positionID in positionVariantDict.keys():
        for variantDict in positionVariantDict[positionID]:
            keys.update(variantDict.keys())
    keys = sorted(list(keys))

    outFile = open(outFilePath, 'w')
    writeMatrixToOpenFile([keys], outFile)

    for positionID in sorted(positionVariantDict):
        for variantDict in positionVariantDict[positionID]:
            outData = [getDictValue(variantDict, key, "NA") for key in keys]
            writeMatrixToOpenFile([outData], outFile)

    outFile.close()

def printStats(description, positionVariantDict, sampleIDs):
    snvPositions = [x for x in positionVariantDict.keys() if x[2] == "SNV"]
    indelPositions = [x for x in positionVariantDict.keys() if x[2] == "INDEL"]
    numSnv = float(sum([len(positionVariantDict[x]) for x in snvPositions]))
    numIndel = float(sum([len(positionVariantDict[x]) for x in indelPositions]))

    printFlush("--------------------------------------------------", outSummaryFilePath)
    printFlush(description, outSummaryFilePath)
    printFlush("Total variants: %i" % (numSnv + numIndel), outSummaryFilePath)
    printFlush("Total positions: %i" % len(positionVariantDict), outSummaryFilePath)
    printFlush("SNV total: %i" % numSnv, outSummaryFilePath)
    printFlush("SNV positions: %i" % len(snvPositions), outSummaryFilePath)
    #printFlush("SNVs per sample: %.3f" % (numSnv / float(len(sampleIDs))), outSummaryFilePath)
    printFlush("SNV positions per sample: %.3f" % (float(len(snvPositions)) / float(len(sampleIDs))), outSummaryFilePath)
    printFlush("Indel total: %i" % numIndel, outSummaryFilePath)
    printFlush("Indel positions: %i" % len(indelPositions), outSummaryFilePath)
    #printFlush("Indels per sample: %.3f" % (numIndel / float(len(sampleIDs))), outSummaryFilePath)
    printFlush("Indels positions per sample: %.3f" % (float(len(indelPositions)) / float(len(sampleIDs))), outSummaryFilePath)

if os.path.exists(outSummaryFilePath):
    os.remove(outSummaryFilePath)

parseVariants()
