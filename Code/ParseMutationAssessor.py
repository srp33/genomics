import os, sys, glob

inFilePattern = sys.argv[1]
outFilePath = sys.argv[2]

outFile = open(outFilePath, "w")

lineCount = 0
outData = []

for maFilePath in glob.glob(inFilePattern):
    maFile = open(maFilePath)
    maFile.readline()

    for line in maFile:
        lineItems = line.rstrip().split("\t")

        if len(lineItems) < 7:
            continue

        chromosome = lineItems[0].split(",")[1]
        position = lineItems[0].split(",")[2]
#        ref = lineItems[0].split(",")[3]
        alt = lineItems[0].split(",")[4]
#        pred = lineItems[6]
        score = lineItems[7]

        outData.append("\t".join([chromosome, position, alt, score]))

        lineCount += 1
        if lineCount % 100000 == 0:
            print chromosome, position
            outFile.write("\n".join(outData) + "\n")
            outData = []

if len(outData) > 0:
    outFile.write("\n".join(outData) + "\n")

outFile.close()
