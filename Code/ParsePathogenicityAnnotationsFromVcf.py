import os, sys, glob
import utilities

inVcfFilePath = sys.argv[1]
queryAnnotationKeys = sys.argv[2].split(",")
outFilePath = sys.argv[3]

missing = "."
queryAnnotationKeySet = set(queryAnnotationKeys)

positionDict = {}

lineCount = 0
for line in file(inVcfFilePath):
    if line.startswith("#"):
        continue

    lineCount += 1
    if lineCount % 10000 == 0:
        print lineCount

    lineItems = line.rstrip().split("\t")

    for alt in lineItems[4].split(","):
        positionID = "%s__%s__%s" % (lineItems[0], lineItems[1], alt)
        if not positionID in positionDict:
            positionDict[positionID] = {}
            for queryAnnotationKey in queryAnnotationKeys:
                positionDict[positionID][queryAnnotationKey] = missing

    for infoItem in lineItems[7].split(";"):
        if "=" not in infoItem:
            continue

        annotationKey = infoItem.split("=")[0].split("__")[0]

        if annotationKey in queryAnnotationKeySet:
            positionDict[positionID][annotationKey] = infoItem.split("=")[1]

outFile = open(outFilePath, 'w')
outFile.write("\t".join(["id"] + queryAnnotationKeys) + "\n")
for positionID in sorted(positionDict):
    scores = [positionDict[positionID][key] for key in queryAnnotationKeys]
    if len([score for score in scores if score != missing]) > 0:
        outFile.write("\t".join([positionID] + scores) + "\n")
outFile.close()
