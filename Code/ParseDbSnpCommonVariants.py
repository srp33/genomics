import os, sys, glob

inFilePath = sys.argv[1]
outFilePath = sys.argv[2]

outFile = open(outFilePath, 'w')

for line in file(inFilePath):
    lineItems = line.split("\t")
    outFile.write("%s__%i\n" % (lineItems[1], int(lineItems[2]) + 1))

outFile.close()
