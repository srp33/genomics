import os, sys, glob

vcfFilePath = sys.argv[1]
outFilePath = sys.argv[2]

commonVariants = set()

lineCount = 0
for line in file(vcfFilePath):
    lineCount += 1

    if lineCount % 100000 == 0:
        print lineCount

    if line.startswith("#"):
        continue

    lineItems = line.rstrip().split("\t")
    isCommon = max([0.0] + [float(x.split('=')[1]) for x in lineItems[7].split(';') if '_AF' in x]) > 0.01

    if isCommon:
        commonVariants.add("chr%s__%s" % (lineItems[0], lineItems[1]))

commonVariants = sorted(list(commonVariants))

outFile = open(outFilePath, 'w')
outFile.write("\n".join(commonVariants) + "\n")
outFile.close()
