import os, sys, glob

vcfFilePattern = sys.argv[1]
outFilePath = sys.argv[2]

commonVariants = set()

for vcfFilePath in glob.glob(vcfFilePattern):
    print vcfFilePath
    for line in file(vcfFilePath):
        if line.startswith("#"):
            continue

        lineItems = line.rstrip().split("\t")

        if lineItems[6] != "PASS":
            continue

        isCommon = False
        for x in lineItems[7].split(";"):
            if x.startswith("MAF="):
                for y in x.replace("MAF=", "").split(","):
                    if float(y) > 1.0:
                        isCommon = True
        if isCommon:
            commonVariants.add("chr%s__%s" % (lineItems[0], lineItems[1]))

commonVariants = sorted(list(commonVariants))

outFile = open(outFilePath, 'w')
outFile.write("\n".join(commonVariants) + "\n")
outFile.close()
