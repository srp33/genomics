import os, sys, glob
import utilities

inVcfFilePath = sys.argv[1]
annotationFilePattern = sys.argv[2]
description = sys.argv[3]
chromosomeColumnIndex = int(sys.argv[4])
positionColumnIndex = int(sys.argv[5])
altColumnIndex = int(sys.argv[6])
scoreColumnIndex = int(sys.argv[7])
outVcfFilePath = sys.argv[8]

chromosomes = set()
chrPositionDict = {}
chrPositionScoreDict = {}

def loadDicts():
    for line in file(inVcfFilePath):
        if line.startswith("#"):
            continue

        lineItems = line.rstrip().split("\t")
        chromosome = lineItems[0].replace("chr", "")
        chromosomes.add(chromosome)

    for chromosome in chromosomes:
        chrPositionDict[chromosome] = set()
        chrPositionScoreDict[chromosome] = {}

    lineCount = 0
    for line in file(inVcfFilePath):
        lineCount += 1
        if lineCount % 10000 == 0:
            print lineCount

        if line.startswith("#"):
            continue

        lineItems = line.rstrip().split("\t")
        chromosome = lineItems[0].replace("chr", "")
        position = int(lineItems[1])
        chrPositionDict[chromosome].add(position)

    for annotationFilePath in glob.glob(annotationFilePattern):
        lineCount = 0
        for line in file(annotationFilePath):
            lineCount += 1
            if lineCount % 100000 == 0:
                print description, os.path.basename(annotationFilePath), lineCount

            lineItems = line.rstrip().split("\t")

            chromosome = lineItems[chromosomeColumnIndex].replace("chr", "")

            if lineCount == 1 and not utilities.isNumeric(chromosome):
                continue

            if not chromosome in chromosomes:
                continue

            position = int(lineItems[positionColumnIndex])
            alt = lineItems[altColumnIndex]
            score = lineItems[scoreColumnIndex]

            if position in chrPositionDict[chromosome]:
                if not chrPositionScoreDict[chromosome].has_key(position):
                    chrPositionScoreDict[chromosome][position] = {}
                chrPositionScoreDict[chromosome][position][alt] = score

loadDicts()

outVcfFile = open(outVcfFilePath, 'w')

lineCount = 0
annotationCount = 0
for line in file(inVcfFilePath):
    if line.startswith("#"):
        outVcfFile.write(line)
        continue

    lineCount += 1
    lineItems = line.rstrip().split("\t")

    chromosome = lineItems[0].replace("chr", "")
    position = int(lineItems[1])
    alts = lineItems[4].split(",")

    if lineCount % 1000 == 0:
        print "Parsing %s - %i" % (chromosome, position)

    for alt in alts:
        if chrPositionScoreDict.has_key(chromosome) and chrPositionScoreDict[chromosome].has_key(position) and chrPositionScoreDict[chromosome][position].has_key(alt):
            values = chrPositionScoreDict[chromosome][position][alt].split(";")

            if len(values) == 1:
                score = values[0]
            else:
                if sum([utilities.isNumeric(x) for x in values]) > 0:
                    score = utilities.calculateMean([float(x) for x in values if x != "."])
                else:
                    score = values[0]

            lineItems[7] += ";%s__%s=%s" % (description, alt, score)
            annotationCount += 1

    outVcfFile.write("\t".join(lineItems) + "\n")

outVcfFile.close()

print "Adding %i annotations for %s" % (annotationCount, description)
