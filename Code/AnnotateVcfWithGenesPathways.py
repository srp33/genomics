import os, sys, glob, shutil, collections, random
from utilities import *
from VariantUtilities import *
from operator import itemgetter, attrgetter

inVcfFilePath = sys.argv[1]
genesBiomartFilePath = sys.argv[2]
exonsBiomartFilePath = sys.argv[3]
geneAltIDsBiomartFilePath = sys.argv[4]
outVcfFilePath = sys.argv[5]

geneStrandDict = {}
geneChrDict = {}
geneExonDict = {}
positionGeneDict = {}

lineCount = 0
for line in file(exonsBiomartFilePath):
    lineItems = line.rstrip().split("\t")

    lineCount += 1
    if lineCount % 1000 == 0:
        print "Parsing %s -- line %i" % (exonsBiomartFilePath, lineCount)

    geneID = lineItems[0]
    start = int(lineItems[2])
    stop = int(lineItems[3])

    geneExonDict[geneID] = geneExonDict.setdefault(geneID, []) + [(start, stop)]

lineCount = 0
for line in file(genesBiomartFilePath):
    lineItems = line.rstrip().split("\t")

    lineCount += 1
    if lineCount % 1000 == 0:
        print "Parsing %s -- line %i" % (genesBiomartFilePath, lineCount)

    geneID = lineItems[0]
    chromosome = parseChromosome(lineItems[4])


    geneChrDict[geneID] = chromosome
    geneStrandDict[geneID] = ["-", "+"][lineItems[3] == "1"]

    positionGeneDict[chromosome] = positionGeneDict.setdefault(chromosome, {})

    for exonPositions in geneExonDict[geneID]:
        for i in range(exonPositions[0] - 2, exonPositions[1] + 3):
            positionGeneDict[chromosome][i] = geneID

geneSymbolDict = {}

geneAltIDsFile = open(geneAltIDsBiomartFilePath)
#Ensembl Gene ID	HGNC symbol	EntrezGene ID
geneAltIDsHeaderItems = geneAltIDsFile.readline().rstrip().split("\t")
print geneAltIDsHeaderItems.index("HGNC symbol")
for line in geneAltIDsFile:
    lineItems = line.split("\t")
    symbol = lineItems[geneAltIDsHeaderItems.index("HGNC symbol")].strip()
    geneSymbolDict[lineItems[0]] = [symbol, "[No Symbol]"][symbol == ""]
geneAltIDsFile.close()

outVcfFile = open(outVcfFilePath, 'w')

genesWithVariants = set()

lineCount = 0
for line in file(inVcfFilePath):
    if line.startswith("#"):
        outVcfFile.write(line)
        continue

    lineItems = line.rstrip().split("\t")

    lineCount += 1
    if lineCount % 1000 == 0:
        print "Parsing %s -- %s, %i" % (inVcfFilePath, chromosome, position)

    chromosome = parseChromosome(lineItems[0])
    position = int(lineItems[1])

    if chromosome in positionGeneDict and position in positionGeneDict[chromosome]:
        geneID = positionGeneDict[chromosome][position]
        strand = geneStrandDict[geneID]

        lineItems[7] += ";GeneID=%s;Strand=%s;GeneSymbol=%s" % (geneID, strand, geneSymbolDict[geneID])
        genesWithVariants.add(geneID)
    else:
        lineItems[7] += ";GeneID=None"

    writeMatrixToOpenFile([lineItems], outVcfFile)

outVcfFile.close()
