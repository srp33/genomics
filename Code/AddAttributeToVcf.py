import os, sys, glob, shutil, collections, random
from utilities import *
from VariantUtilities import *
from operator import itemgetter, attrgetter

inVcfFilePath = sys.argv[1]
attributeDescription = sys.argv[2]
attributeFilePath = sys.argv[3]
outVcfFilePath = sys.argv[4]

positionIDs = set([line.rstrip() for line in file(attributeFilePath)])

outVcfFile = open(outVcfFilePath, 'w')

lineCount = 0
for line in file(inVcfFilePath):
    if line.startswith("#"):
        outVcfFile.write(line)
        continue

    lineCount +=1
    if lineCount % 10000 == 0:
        print lineCount

    lineItems = line.rstrip().split("\t")

    chromosome = parseChromosome(lineItems[0])
    position = int(lineItems[1])
    positionID = "%s__%i" % (chromosome, position)

    value = "False"
    if positionID in positionIDs:
        value = "True"

    lineItems[7] += ";%s=%s" % (attributeDescription, value)
    outVcfFile.write("\t".join(lineItems) + "\n")

outVcfFile.close()
