import os, sys, glob, shutil, collections, random
import utilities
from operator import itemgetter, attrgetter

genomeFastaFilePath = sys.argv[1]
entrezGenePositionsFilePath = sys.argv[2]
outFilePath = sys.argv[3]

def processGenesOnChromosome(chromosome, sequence):
    print "Processing genes on %s" % chromosome

    for geneID in [x for x in genePositionsDict if genePositionsDict[x][0] == chromosome]:
        startPosition = genePositionsDict[geneID][1] - 1
        stopPosition = genePositionsDict[geneID][2] - 1
        maxPosition = max([startPosition, stopPosition])

        if len(sequence) >= maxPosition:
            geneSequence = sequence[(startPosition-1):stopPosition]
            outFile.write("%i\t%s\n" % (geneID, geneSequence))

genePositionsDict = {}

for line in file(entrezGenePositionsFilePath):
    lineItems = line.rstrip().split("\t")
    geneID = int(lineItems[0])
    chromosome = lineItems[1]
    start = int(lineItems[2])
    stop = int(lineItems[3])

    genePositionsDict[geneID] = (chromosome, start, stop)

outFile = open(outFilePath, 'w')
fastaFile = open(genomeFastaFilePath)

currentChromosome = ""
for line in fastaFile:
    if line.startswith(">"):
        if currentChromosome != "":
            processGenesOnChromosome(currentChromosome, sequence)

        currentChromosome = line.rstrip()[1:]
        sequence = ""
    else:
        sequence += line.rstrip().upper()
        continue

processGenesOnChromosome(currentChromosome, sequence)

fastaFile.close()
outFile.close()
