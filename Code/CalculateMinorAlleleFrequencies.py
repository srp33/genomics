import os, sys, glob, shutil, collections, random
from utilities import *
from VariantUtilities import *
from operator import itemgetter, attrgetter

inVcfFilePath = sys.argv[1]
outVcfFilePath = sys.argv[2]

positionFrequencyDict = {}
uniqueSampleIDs = set()

inVcfFile = open(inVcfFilePath)
line = inVcfFile.readline()
while line.startswith("##"):
    line = inVcfFile.readline()

sampleIDs = line.rstrip().split("\t")[9:]
for sampleID in sampleIDs:
    uniqueSampleIDs.add(sampleID)

lineCount = 0
for line in inVcfFile:
    lineCount += 1

    parseResult = parseVcfLine(line, sampleIDs, None)
    if parseResult == None:
        continue
    positionID, chromosome, position, externalID, ref, variantType, genotypeValuesList, geneAnnotationDict, variantAnnotationDict = parseResult

    if lineCount % 10000 == 0:
        printFlush("Parsing variant info for %s - %i" % (chromosome, position))

    if positionID not in positionFrequencyDict:
        positionFrequencyDict[positionID] = {"HC": 0, "HT": 0, "HR": 0, "UK": 0}

    for i in range(len(genotypeValuesList)):
        description = genotypeValuesList[i][1]
        positionFrequencyDict[positionID][description] += 1

inVcfFile.close()

positionMafDict = {}

for positionID in positionFrequencyDict:
    minorAlleleDescription = getMinorAlleleDescription(positionFrequencyDict[positionID])
    majorAlleleDescription = ["HC", "HR"][minorAlleleDescription == "HC"]

    numSamplesWithData = sum(positionFrequencyDict[positionID].values())
    positionFrequencyDict[positionID][majorAlleleDescription] += (len(uniqueSampleIDs) - numSamplesWithData)
    positionMafDict[positionID] = calculateNonRefAlleleFrequency(positionFrequencyDict[positionID])

outVcfFile = open(outVcfFilePath, 'w')

for line in file(inVcfFilePath):
    if line.startswith("#"):
        outVcfFile.write(line)
        continue

    parseResult = parseVcfLine(line, sampleIDs, None)
    if parseResult == None:
        continue
    positionID, chromosome, position, externalID, ref, variantType, genotypeValuesList, geneAnnotationDict, variantAnnotationDict = parseResult
    maf = positionMafDict[positionID]

    lineItems = line.rstrip().split("\t")
    lineItems[7] += ";LocalMAF=%.4f" % maf
    outVcfFile.write("\t".join(lineItems) + "\n")

outVcfFile.close()
